/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.mavenproject1;

import javax.swing.*; 
import java.awt.*;  
import java.awt.event.*;  
public class JLabelActionListener extends Frame implements ActionListener{  
    JTextField t; JLabel l; JButton b;  
    JLabelActionListener(){
        JFrame f= new JFrame("Label IP");  
        t=new JTextField();  
        t.setBounds(50,50, 150,20);  
        l=new JLabel();  
        l.setBounds(50,100, 250,20);      
        b=new JButton("Find IP");  
        b.setBounds(50,150,95,30);  
        b.addActionListener(this);    
        f.add(b);
        f.add(t);
        f.add(l);    
        f.setSize(400,400);  
        f.setLayout(null);  
        f.setVisible(true);  
    }  
    public void actionPerformed(ActionEvent e) {  
        try{  
        String host=t.getText();  
        String ip=java.net.InetAddress.getByName(host).getHostAddress();  
        l.setText("IP of "+host+" is: "+ip);  
        }catch(Exception ex){System.out.println(ex);}  
    }  
    public static void main(String[] args) {  
        new JLabelActionListener();  
    } }  