/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.mavenproject1;

import javax.swing.*;

/**
 *
 * @author Satit Wapeetao
 */
public class JSliderpaintingticks extends JFrame {

    public JSliderpaintingticks() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JSliderpaintingticks frame = new JSliderpaintingticks();
        frame.pack();
        frame.setVisible(true);
    }
}
